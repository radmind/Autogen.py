#!/usr/bin/env python
# Note: Tested only on Python3
# Note 2: Before using you require a Goodreads Developer Access Key and Secret from here: http://goodreads.com/api
# And place the corresponding values to the variables below
#
# Requirements:
#   goodreads
#   pyperclip (Optional, required to copy description to clipboard)
#
# Install using:
#   pip install goodreads
#   pip install pyperclip
#
# Usage: python3 this_file.py ISBN
# 

import argparse
from goodreads import client

GR_ACCESS_KEY = "####################"
GR_ACCESS_KEY_SECRET = "#######################"

parser = argparse.ArgumentParser()

parser.add_argument('isbn', metavar='ISBN', help='ISBN of the book to search for on Goodreads')
args = parser.parse_args()



gc = client.GoodreadsClient(GR_ACCESS_KEY, GR_ACCESS_KEY_SECRET)

gc.authenticate(GR_ACCESS_KEY, GR_ACCESS_KEY_SECRET)


b = gc.book(isbn=args.isbn)

d = b.description.replace('<br />', '\n').replace('<b>', '[b]').replace('</b>', '[/b]').replace('<i>', '[i]').replace('</i>', '[/i]')
upload_str = '''{7}
{8}
[size=3][b]Book Details:[/b][/size]
[quote]
[b]ISBN:[/b] {0} {5} ({6})
[b]Publisher:[/b] {1} 
[b]Publication Year:[/b] {2} 
[b]Website:[/b] [url={3}]Goodreads[/url]
[/quote]

[size=3][b]About the Book:[/b][/size]
[quote]
{4}
[/quote]
'''.format(b.isbn, b.publisher, b.publication_date[2], b.link, d, b.isbn13, b.format, b.title, ', '.join(str(author) for author in b.authors))

print("Goodreads Link: {}".format(b.link))
print(upload_str)

try:
    import pyperclip
    pyperclip.copy(upload_str)
    print('Description copied to clipboard.')
except ImportError as e:
    pass
